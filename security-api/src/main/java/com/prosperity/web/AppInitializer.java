package com.prosperity.web;

import javax.servlet.Filter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer implements WebApplicationInitializer {

	public void onStartup(ServletContext container) throws ServletException {
		System.err.println("STARTING Prosperity WEBAPP");
		
		AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
		ctx.register(WebConfiguration.class);
		ctx.setServletContext(container);

		ServletRegistration.Dynamic servlet = container.addServlet(
				"dispatcher", new DispatcherServlet(ctx));

		servlet.setLoadOnStartup(1);
		servlet.addMapping("/prosperity/*");
		servlet.addMapping("/");

		System.err.println("Prosperity Startup Init Complete");
	}

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[]{AppBeans.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] {WebConfiguration.class};
	}

	@Override
	protected String[] getServletMappings() {
		return new String[]{
				 "/"
		};
	}

	@Override
	protected Filter[] getServletFilters() {
		
		CharacterEncodingFilter charFilter = new CharacterEncodingFilter();
		charFilter.setEncoding("UTF-8");
		charFilter.setForceEncoding(true);
				
		return new Filter[] { 
			  charFilter
		};
	}
	

    void registerWebContext( AnnotationConfigWebApplicationContext appContext, ServletContext servletContext ) {
        appContext.scan( "prosperity" );
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet( "prosperityDispatcherServlet", new DispatcherServlet( appContext ) );
        dispatcher.setLoadOnStartup( 1 );
        dispatcher.addMapping( "/" );
    }
}