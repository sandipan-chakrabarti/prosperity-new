package com.prosperity.web;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoDbUtils;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;

@Configuration
@PropertySource(value = { "classpath:conf/persistence.props" })
public class AppBeans {

	private static final String PROSPERITY = "prosperity";

	@Value("${MONGO.HOST}")
	private String mongoHost = "REQUIRED_VALUE";

	@Value("${MONGO.PORT}")
	private int mongoPort = Integer.MIN_VALUE;	

	@Bean
	public static  PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
	@Bean(name="mongoTemplate",autowire=Autowire.BY_NAME)
	public MongoTemplate getMongoTemplate() {
		
		return new MongoTemplate(getMongoDbFactory());
	}

	@Bean(name="mongoDbFactory")
	public MongoDbFactory getMongoDbFactory() {
		  return new SimpleMongoDbFactory(getMongoClient(), PROSPERITY);
	}
	
	@Bean(name="mongoClient")
	public MongoClient getMongoClient() {
		return new MongoClient(mongoHost, mongoPort);
	}
}
