package com.prosperity.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.prosperity.api.service.UserService;

@RestController
@RequestMapping(value="/auth")
public class AuthController {
	
	@Autowired private UserService userService;
	
	@RequestMapping(value="/validate")
	public boolean validateUser(
			  @RequestParam(name="key",required=true) String key
			 ,@RequestParam(name="userId",required=true) String email 
			) {
		
		return userService.authenticate(email, key);
	
	}

}







