package com.prosperity.api.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value="/")
public class AppEntryController {
	private final Logger logger = LoggerFactory.getLogger(AppEntryController.class);
	
	@RequestMapping
	public String launchApp(HttpServletRequest request) {
		logger.warn("Index Page was Requested from user @ : {}",request.getRemoteHost());;
		
		return "index";
	}
	
}
