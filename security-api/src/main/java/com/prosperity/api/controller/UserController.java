package com.prosperity.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.prosperity.api.service.UserService;
import com.prosperity.dao.model.User;

@RestController
@RequestMapping(value="/users")
public class UserController {

	@Autowired private UserService userService;
	
	@RequestMapping(value="/add", method = RequestMethod.GET)
    public User addUser(
    			 @RequestParam(name="email",required=true) String email
    			,@RequestParam(name="password",required=true) String password 
    		) {
    	
    	return userService.addUser(email, password);
    	
    }
    
	@RequestMapping(value="/delete", method = RequestMethod.GET)
    public void addUser(
    			 @RequestParam(name="email",required=true) String email
    		) {
    	
    	userService.deleteUser(email);
    }
	
}
