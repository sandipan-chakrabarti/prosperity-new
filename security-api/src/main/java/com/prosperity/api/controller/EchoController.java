package com.prosperity.api.controller;

import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.prosperity.dao.model.Greeting;

@RestController
public class EchoController {
	private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    private final Logger logger = LoggerFactory.getLogger(EchoController.class);
    
    @RequestMapping(value="/greeting",method={RequestMethod.GET,RequestMethod.POST})
    public Greeting greet(@RequestParam(value="name", defaultValue="World") String name) {
    	logger.info("Greeting Called oN Server ");
    	
    	return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }
    
}
