package com.prosperity.api.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.prosperity.api.repo.UserHashRepository;
import com.prosperity.api.repo.UserRepository;
import com.prosperity.dao.model.User;
import com.prosperity.dao.model.UserHash;

public class UserService {

	private final Logger logger = LoggerFactory.getLogger(UserService.class);
	
	@Autowired UserRepository userRepository;
	@Autowired UserHashRepository userHashRepository;
	
	public boolean authenticate(String email,String key){
		logger.info("UserAuth Requested for User :{} ",email);
		
		UserHash hash = userHashRepository.getOneByUserEmail(email);
		
		return hash.getHash().equalsIgnoreCase(key);
	}
	
	public User addUser(String email,String password) {
		logger.info("Adding New User Withe Emails : {} ",email);
		
		UserHash hash = userHashRepository.getOneByUserEmail(email);
    	if(hash == null) {
    		UserHash userHash = new UserHash();
    		
    		userHash.setUserEmail(email);
    		userHash.setHash(password);
    		
    		userHashRepository.insert(userHash);
    		logger.debug("User Hash Saved , Proceeding to save User");
    		
    		User userToAdd = new User();
    	
    		userToAdd.setEmail(email);
        	userRepository.addUser(userToAdd);

        	logger.info("User Save Sucessful");
        	return userToAdd;
    	} else {
    		logger.error("Email already attached to user :{}",email);
    		throw new RuntimeException("User with same email already Exists");
    	}
    	
	}
	
	public void deleteUser(String email) {
		
		this.userRepository.deleteUser(email);
	}
}
