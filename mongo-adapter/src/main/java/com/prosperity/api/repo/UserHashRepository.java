package com.prosperity.api.repo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.prosperity.dao.model.UserHash;


@Component
@Repository(value="userHashRepository")
public class UserHashRepository extends CommonRepository<UserHash> {
	Logger logger = LoggerFactory.getLogger(UserHashRepository.class);

	public UserHashRepository() {
		super(UserHash.class);
	}
	
	public UserHash getOneByUserEmail(String email ){
		Query query = new Query();
		query.addCriteria(Criteria.where("email").is(email));
		
		return getOneByQuery(query);		
	}
	
	public UserHash addUserHash(UserHash userHash){
		logger.info("Adding New User Hash To Collection : {}"+userHash);
		insert(userHash);
		
		return userHash;
	}
	
}
