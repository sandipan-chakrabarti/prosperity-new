package com.prosperity.api.repo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.mongodb.WriteResult;
import com.prosperity.dao.model.Model;

public abstract class CommonRepository<Type extends Model> {
	
	Logger logger = LoggerFactory.getLogger(CommonRepository.class);

	@SuppressWarnings("rawtypes")
	final Class claxx;
	
	@Autowired(required=true)
	private MongoTemplate mongoTemplate;

	protected CommonRepository(@SuppressWarnings("rawtypes") Class claxx) {
		this.claxx = claxx;
	}

	/**
	 * MAKING PRIVATE IN ORDER TO CONSERVE ACCIDENTAL CALL
	 * @return
	 */
	private MongoTemplate getMongoTemplate(){
		MongoTemplate overridden = getOverriddenMongoTemplate();
		if(overridden != null){
			return overridden;
		}

		return this.mongoTemplate;
	}

	/**
	 * Subclasses can Override this to Provide a custom Template
	 * @return
	 */
	protected MongoTemplate getOverriddenMongoTemplate(){
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<Type> getAll(){
		return getMongoTemplate().findAll(this.claxx);
	}

	public Type getOneById(String id){
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));

		return getOneByQuery(query);
	}

	@SuppressWarnings("unchecked")
	public Type getOneByQuery(Query query) {
		logger.debug("Finding Object By Query : %s",query);
		return (Type) getMongoTemplate().findOne(query,this.claxx);
	}

	@SuppressWarnings("unchecked")
	public List<Type> getAllByQuery(Query query){
		logger.debug("Finding All Objects matching Query :%s",query);
		
		return getMongoTemplate().find(query, this.claxx);
	}

	public Type update(Type value){
		return updateImpl(value);
	}

	protected String buildKey(String bucket,Type value){
		StringBuffer sb = new StringBuffer();
		
		sb.append(value.getClass().getCanonicalName());
		sb.append(":");
		sb.append(bucket);
		sb.append(":");
		sb.append(value.getId());
		logger.debug("Key Generated for Locking :%s",sb.toString());
		
		return sb.toString();
	}
	
	protected Type updateImpl(final Type value){

		logger.debug("Value for Update :%s",value);
		if( value == null || value.getId() == null){
			logger.error("Invalid Object for save :%s",value);
			throw new RuntimeException("Invalid Object Id , cant  update non existing objects , try insert");
		}
		
		final MongoTemplate template = getMongoTemplate();
		logger.info("Version Match Passed , continuing for update");
		template.save(value);		
			
		
		return value;				
	}

	protected void setValue(String id,String name,Object value) {
		WriteResult result = getMongoTemplate().updateFirst(
				Query.query(Criteria.where("id").is(id)), 
				Update.update(name, value),
				this.claxx);

		logger.debug("Return Result :%s",result);
		if(1 != result.getN()){
			throw new RuntimeException("Error Updating Value");
		}
	}

	public Type insert(Type value) {
		logger.debug("Inserting Value to Db :%s",value);

		if(value.getId() != null){
			throw new RuntimeException("Cannot Insert Object with DB ID Already Set");
		}

		getMongoTemplate().insert(value);
		logger.debug("Value Stored Persisted to DB :%s",value);

		return value;
	}

	public void delete(Type value){
		logger.info("Removing Object :{} ",value.getId() );
		getMongoTemplate().remove(value);
	}
	
}
