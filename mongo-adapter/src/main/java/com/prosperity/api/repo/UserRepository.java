package com.prosperity.api.repo;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.prosperity.dao.model.User;

@Component
@Repository(value="userRepository")
public class UserRepository extends CommonRepository<User> {
	Logger logger = LoggerFactory.getLogger(UserRepository.class);

	public UserRepository() {
		super(User.class);
	}
		
	public User getUserById(String userId){
		logger.info("Searching User by Id :{}",userId);
		
		return getOneById(userId);
	}
	
	public List<User> findAll(Pageable arg0) {
		Query query = new Query();
		
		return getAllByQuery(query);
	}
	
	public List<User> findAll(String firstName,String lastName){
		logger.info("Searching User by firstName & LastName :{} | {}",firstName,lastName);
		
		Query query = new Query();
		
		List<User> users = getAllByQuery(query);
		
		List<User> retValue = new ArrayList<User>();
		for(User user : users){
			if(firstName != null && firstName.trim().length() > 0){
				if(! user.getFirstName().contains(firstName)) {
					continue;
				}
			}
		
			if(lastName != null && lastName.trim().length() > 0){
				if(! user.getLastName().contains(lastName)) {
					continue;
				}
			}
			
			retValue.add(user);
		}
		
		return retValue;
	}
	
	public User addUser(User user){
		logger.info("Adding New User To Collection : {}",user);
		validateUser(user);
		
		insert(user);
		
		return user;
	}
	
	public void deleteUser(String email){
		logger.info("Deleting User from collection :{}",email);
		
		delete(getOneById(email));
	}

    private void validateUser(User userToAdd){ 
    	//TODO Implement User Parameters Validation here
    }

}
